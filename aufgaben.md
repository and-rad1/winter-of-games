**Übungsaufgaben (Andreas)**

1. Aufgabe: Klonen eines GitLab-Projekts
   - Übung: Jeder Teilnehmer klont das Projekt, erstellt einen neuen Branch und fügt ein neues Rezept in einer Markdown-Datei hinzu. Nach dem Hinzufügen des Rezepts führt der Teilnehmer einen Commit durch und pusht die Änderungen auf ihren Branch.

2. Aufgabe: Erstellen einer Merge-Anfrage
   - Übung: Der Teilnehmer erstellt eine Merge-Anfrage, um sein Rezept zum Hauptbranch hinzuzufügen. Sie geben eine Beschreibung ihrer Änderungen an und markieren einen anderen Teilnehmer für eine Code- (oder in diesem Fall Rezept-) Überprüfung.

3. Aufgabe: Überprüfen eines Rezepts
   - Übung: Die Teilnehmer überprüfen die Merge-Anfrage des Rezepts, in dem sie markiert wurden. Sie geben Feedback, Vorschläge oder Genehmigungen in der Diskussion zur Merge-Anfrage. Wenn Änderungen erforderlich sind, nimmt der ursprüngliche Teilnehmer die Änderungen in ihrem Branch vor und aktualisiert die Merge-Anfrage.

4. Aufgabe: Lösen von Merge-Konflikten
   - Übung: Die Teilnehmer führen absichtlich widersprüchliche Änderungen in separaten Rezepten oder im gleichen Rezept ein. Sie versuchen dann, die Rezepte zusammenzuführen und die Konflikte mit Hilfe der Merge-Anfragenoberfläche von GitLab zu lösen.
