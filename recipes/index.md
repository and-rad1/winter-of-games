# Inhaltsverzeichnis

## Vorspeisen
1. [Feldsalat mit Granatapfel und Ziegenkäse](link-zum-rezept)
2. [Maronensuppe mit Trüffelöl](link-zum-rezept)
3. [Lachs-Tartar auf Pumpernickel](link-zum-rezept)

## Hauptgerichte
1. [Gänsebraten mit Apfel-Rotkohl und Klößen](link-zum-rezept)
2. [Karpfen blau mit Kartoffelsalat](link-zum-rezept)
3. [Vegetarische Nuss-Roulade mit Pilzfüllung](link-zum-rezept)

## Desserts
1. [Bratapfel mit Vanillesauce](link-zum-rezept)
2. [Lebkuchen-Tiramisu](link-zum-rezept)
3. [Zimtsterne](link-zum-rezept)

## Getränke
1. [Glühwein](link-zum-rezept)
2. [Apfelpunsch](link-zum-rezept)
3. [Eierlikör](link-zum-rezept)