# Weihnachtsrezeptbuch

## Aufgaben/Übungen

1. **Aufgabe: Erstellen Sie eine Zusammenführungsanforderung**
   - Übung: Der Teilnehmer erstellt einen Merge Request, um sein Rezept dem Hauptzweig hinzuzufügen. Sie beschreiben ihre Änderungen und markieren einen anderen Teilnehmer für eine Überprüfung des Codes (oder in diesem Fall des Rezeptes).

2. **Aufgabe: Überprüfen eines Rezeptes**
   - Übung: Die Teilnehmer überprüfen die Zusammenführungsanfrage des Rezepts, in dem sie markiert wurden. Sie geben in der Diskussion über die Zusammenführungsanforderung Feedback, Vorschläge oder Zustimmung. Wenn Änderungen erforderlich sind, nimmt der ursprüngliche Teilnehmer die Änderungen in seinem Zweig vor und aktualisiert die Zusammenführungsanforderung.

3. **Aufgabe: Merge-Konflikte auflösen**
   - Übung: Die Teilnehmer führen absichtlich widersprüchliche Änderungen in verschiedenen Rezepten oder im selben Rezept ein. Anschließend versuchen sie, die Rezepte zusammenzuführen und die Konflikte mithilfe der GitLab-Schnittstelle für Zusammenführungsanfragen zu lösen.


## Ablauf Übungsphase

1. **Einführung**
   - In diesem Projekt verwenden wir GitLab, um gemeinsam an einem Rezeptbuch zu arbeiten. Jeder Teilnehmer wird ein eigenes Rezept hinzufügen und die Beiträge der anderen überprüfen. Wir verwenden GitLab-Funktionen wie Repositories, Branches, Merge-Anfragen und Code-Reviews.

2. **Projekteinrichtung**
   - Sie können das Projekt klonen, indem Sie die URL des GitLab-Projekts in der Befehlszeile mit `git clone` verwenden. Erstellen Sie einen neuen Branch mit `git checkout -b IhrBranchName`.

3. **Hinzufügen eines Rezepts**
   - Richtlinien zum Hinzufügen eines Rezepts (Dateiformat, einzuschließende Informationen usw.): Fügen Sie Ihr Rezept in einer Markdown-Datei hinzu. Stellen Sie sicher, dass Sie den Namen des Rezepts, die Zutaten, die Schritte und die Zubereitungszeit angeben.

4. **Erstellen einer Merge-Anfrage**
   - Nachdem Sie Ihre Änderungen vorgenommen haben, können Sie mit `git push origin IhrBranchName` Ihre Änderungen pushen und dann auf GitLab eine Merge-Anfrage erstellen. Vergessen Sie nicht, einen Reviewer zu markieren.

5. **Überprüfen eines Rezepts**
   - Richtlinien zum Überprüfen eines Rezepts und zur Rückmeldung: Überprüfen Sie das Format, die Klarheit und die Genauigkeit des Rezepts. Geben Sie konstruktives Feedback und Vorschläge zur Verbesserung.

6. **Lösen von Merge-Konflikten**
   - Anleitung zum Lösen von Merge-Konflikten in GitLab: Merge-Konflikte treten auf, wenn zwei Branches Änderungen an den gleichen Codezeilen vornehmen. Sie können diese Konflikte lösen, indem Sie die Konfliktmarkierungen in Ihrer Datei suchen und entscheiden, welche Änderungen beibehalten werden sollen.