# GitLab Workshop: Zusammenarbeit mit GitLab (Weihnachtsrezeptbuch-Projekt)

## Einstieg
1. Begrüßung der Teilnehmer
2. Vorstellung der Workshop-Ziele und des Ablaufplans
3. Kurze Vorstellungsrunde oder Icebreaker-Aktivität, um die Teilnehmer miteinander vertraut zu machen

## Arbeitsphase
1. [Einführung](einfuerung-in-git-und-gitlab) in GitLab und Git-Grundlagen
   - Überblick über die Funktionen und Vorteile von GitLab
   - Verständnis der GitLab-Oberfläche und Navigation
   - Grundlegende Git-Befehle und -Konzepte (Commit, Branch, Merge usw.)
   - Verständnis von Git-Workflows (zentralisiert, Feature Branch, Gitflow)

2. [Aufgaben](Aufgaben)

- Erstellen eines GitLab-Projekts
- Erstellen einer Merge-Anfrage
- Überprüfen eines Rezepts
- Lösen von Merge-Konflikten

## Phase der Sicherung

1. Klärung von Fragen
   - In diesem Abschnitt geht es darum, offene Fragen und Unklarheiten zu klären. Das Ziel ist es, sicherzustellen, dass alle Teilnehmerinnen und Teilnehmer ein klares Verständnis der behandelten Themen haben.

2. Wiederholung der Schlüsselkonzepte
   - Hier liegt der Fokus darauf, die wichtigsten GitLab-Konzepte und -Funktionen zu wiederholen, die während des Workshops eingesetzt wurden. Dies dient dazu, das Verständnis zu vertiefen und sicherzustellen, dass die Teilnehmerinnen und Teilnehmer die Konzepte in der Praxis anwenden können.

3. Rückblick und Reflektion
   - Ziel ist es, dass die Teilnehmerinnen und Teilnehmer ihre persönlichen Erfahrungen und Erkenntnisse aus den Übungen und Aufgaben miteinander teilen. Dies fördert das gemeinsame Lernen und den Austausch von Best Practices.

4. Selbstbewertung
   - Ziel dieses Abschnitts ist es, dass die Teilnehmerinnen und Teilnehmer ihr eigenes Verständnis und ihre Fähigkeiten in Bezug auf die im Workshop behandelten Themen einschätzen. Dies fördert die Selbstreflexion und hilft den Teilnehmerinnen und Teilnehmern, Bereiche zu identifizieren, in denen sie möglicherweise weitere Unterstützung oder Praxis benötigen.

## Abschluss
1. Abschlussdiskussion und Feedback
   - Diskussion über die Erfahrungen der Teilnehmer und das Endergebnis des Projekts
   - Sammeln von Feedback zur Verbesserung zukünftiger Workshops

2. Zusätzliche Ressourcen
   - Bereitstellung von Links zu relevanten GitLab-Dokumentationen, Tutorials oder externen Ressourcen für weiteres Lernen
