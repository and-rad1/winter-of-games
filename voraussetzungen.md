## Voraussetzungen

1. Grundkenntnisse in Git und Versionskontrollkonzepten.
2. Vertrautheit mit der Befehlszeilenschnittstelle (CLI) zur Ausführung von Git-Befehlen.
3. Zugang zu einem Computer mit einem modernen Webbrowser und Internetverbindung.
4. Jeder Teilnehmer sollte ein eigenes GitLab-Konto haben (entweder ein persönliches Konto oder Zugang zu einem gemeinsamen Konto, das für den Workshop erstellt wurde).
