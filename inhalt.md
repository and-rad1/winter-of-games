Kollaboratives Arbeiten mit Git und GitLab

**GitLab Workshop Syllabus: Collaboration with GitLab (Condensed Version)**

Duration: 3 hours

**Introduction to GitLab and Basic Git Concepts (Isabel)**

1. Introduction to GitLab 
   - Overview of GitLab features and benefits
   - Understanding the GitLab interface and navigation

2. Basic Git Workflows
   - ~~Introduction to version control and Git~~
   - ~~Basic Git commands and concepts (commit, branch, merge, etc.)~~
   - Understanding Git workflows (centralized, feature branch, Gitflow)

3. ~~Collaborating with GitLab~~
   - ~~Cloning a GitLab project~~
   - ~~Making changes and committing them~~
   - ~~Pushing changes to the remote repository~~
   - ~~Pulling changes from the remote repository~~

4. Branching and Merging
   - Creating and managing branches in GitLab
   - Understanding merge requests and their purpose
   - Resolving merge conflicts

5. Code Reviews with Merge Requests
   - Creating and managing merge requests in GitLab
   - Reviewing and commenting on code changes
   - Approving and merging merge requests
